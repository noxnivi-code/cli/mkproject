#!/usr/bin/env python

# mkfiles.py
# (mkproject)
# (C) 202306 NoxNivi
# script to append content to the files 
#   AUTHORS
#   TODO.md
#   __main__.py

# from imports
from pathlib import Path
from datetime import datetime


def date_today():
    """
    Returns the year and month when this script is run in the format
    YYYYMM
    """
    today_ym = datetime.now()
    today_formatted = today_ym.strftime('%Y%m')

    return today_formatted


def add_authors(projname, authfile):
    """
    Adds content to the AUTHORS file
    """

    authors_skel = (f"""# This is the list of <{projname}>'s significant contributors
""")

    authfile.write_text(authors_skel)


def add_todo(projname, todofile):
    """
    Adds content to the TODO.md fiel
    """

    todomd_skel = (f"""# **{projname}**'s TODO list
""")

    todofile.write_text(todomd_skel)


def add_python_main_skel(projname, mainfile):
    """
    Adds the python shebang to the __main__.py file and some lines to 
    form a base python script
    """

    # TODO: evaluate the skeleton to add to main file
    # shebang_line = '#!/usr/bin/env python'

    today_formatted = date_today()
    base_python_skel = (f"""#!/usr/bin/env python

# __main__.py
# {projname} v.
# (C) {today_formatted} <author>
#
# Add description here

# base imports

# local imports

# from imports

if __name__ == "__main__":
    print('{projname}')
""")
    mainfile.write_text(base_python_skel)


def add_nim_main_skel(projname, mainfile):
    """
    Adds a basic Python structure for the software
    """
    
    today_formatted = date_today()
    base_nim_skel = (f"""# main.nim
# {projname} v. 0.1.0
# (C) {today_formatted} <author>
#
# Add description here
#

# base imports

# local imports


proc mainProc() =
  #[
    The main procedure
  ]#

  echo "{projname}"


when isMainModule:
  mainProc()
  GC_fullcollect()
""")


def add_qml_main_skel(projname, mainfile):
    """
    Adds a basic QML sctructure for the software's main window
    """

    today_formatted = date_today()
    base_qml_skel = (f"""// main.qml
// {projname} v.
// (C) {today_formatted} <author>
//
// Add description here

// base imports
import QtQuick
import QtQuick.Window
import QtQuick.Controls
import QtQuick.Layouts

// local imports
""")

    window_qml_skel = ("""
Window {
  id: root
  width: 450
  height: 250

  Component.onCompleted: visibility = true
}
""")

    base_qml_skel = (base_qml_skel + window_qml_skel)

    mainfile.write_text(base_qml_skel)


def add_pythonq_main_skel(projname, mainfile):
    """
    Adds a basic Python structure for the software if QML
    """

    today_formatted = date_today()
    base_pythonq_skel = (f"""#!/usr/bin/env python

# __main__.py
# {projname} v.
# (C) {today_formatted} <author>
#
# Add description here

# base imports
import sys
import importlib.resources

# local imports

# from imports
from PySide6.QtGui import QtGuiApplication
from PySide6.QtQml import QQmlApplicationEngine, QmlElement
from Pyside6.QtCore import Signal, QObject, Slot, Property

# Bridge class that connects signals and slots with the QML GUI
# NOTE: Bridge class might be removed depending on project needs
QML_IMPORT_NAME = "<add namespace here>.<add module here>"
QML_IMPORT_MAJOR_VERSION = 1


if __name__ == "__main__":
    print('{projname}')
    app = QGuiApplication(sys.argv)
    engine = QQmlApplicationEngine()
    # to quit the app from the menu?
    engine.quit.connect(app.quit)

    qml_gui_file = importlib.resources.files("gui").joinpath("main.qml")
    qml_file = importlib.resources.as_file(qml_gui_file)

    with qml_gui_file as qfile:
        engine.load(qfile)

    bridge = Bridge()
    engine.rootObjects()[0].setProperty('bridge', bridge)
    if not engine.rootObjects():
        sys.exit(-1)

    exit_code = app.exec()
    del engine
    sys.exit(exit_code)
""")

    mainfile.write_text(base_pythonq_skel)


def add_nimq_main_skel(projname, mainfile):
    """
    Adds a basic Nim structure for the software if QML
    """

    today_formatted = date_today()
    base_nimq_skel = (f"""# main.nim
# {projname} v. 0.1.0
# (C) {today_formatted} <author>
#
# Add description here
#

# base imports
import nimqml
import std/os

# local imports


proc mainProc() =
  #[
    The main procedure
    - loads the QML engine and resources and starts the application
    - sets the context(s) property(s) for the QML engine
  ]#

  let app = newQApplication()
  defer: app.delete()

  let exit_logic = newExitLogic(app)
  defer: exit_logic.delete()

  let engine = newQQmlApplicationEngine()
  defer: engine.delete()

  let logic_variant = newQVariant(exit_logic)
  defer: logic_variant.delete()

  engine.setRootContextProperty("exit_logic", logic_variant)

  let resources_file = joinPath(app.applicationDirPath, "resources.rcc")
  QResource.registerResource(resources_file)
  engine.load(newQUrl("qrc:///gui/main.qml"))
  app.exec()


when isMainModule:
  mainProc()
  GC_fullcollect()
""")

    mainfile.write_text(base_nimq_skel)


def add_desktop_file(projname, mainfile):
    """
    Creates a basic .desktop content to be tweaked later
    """

    appname = projname.capitalize()

    base_desktop_skel = f"""[Desktop Entry]
Type=Application
## Desktop entry specification version 
Version=1.0
Name={appname}
## add a comment here (tooltip)
Comment=This is a comment
## path to folder where executable is located (and args)
Path=/path/to/folder
## executable of the program
Exec={projname}
## name of icon file.. defaults to 'application-default-icon'
## should be 'projname' all lowercase
Icon=application-default-icon
## set true if needs to run in a Terminal
Terminal=false
## categories to organize apps
Categories=Education;Languages;
"""

    mainfile.write_text(base_desktop_skel)


def add_help_file(projname, mainfile):
    """
    Creates a basic HTML file to be the template on where to build the
    program's help
    """

    help_file_skel = f""" <!DOCTYPE html>
<html lang="en">
<meta charset="UTF-8">
<title>{projname}'s Help'</title>
<meta name="viewport" content="width=device-width,initial-scale=1">
<link rel="stylesheet" href="">
<style>
</style>
<script src=""></script>
<body>

<div class="">
 <h1>{projname}'s Help</h1>
 <p>This is a paragraph.</p>
 <p>This is another paragraph.</p>
</div>

</body>
</html>
"""

    mainfile.write_text(help_file_skel)
