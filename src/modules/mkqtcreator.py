#!/usr/bin/env python

# mkqtcreator.py
# (mkproject)
# (C) 202306 NoxNivi
# script to create QtCreator project files
#

from pathlib import Path


def add_project_file(projname, language):
    """
    Creates the <projectname>.XXXproject file with info on the project files
        projectname.pyproject   for Python
        projectname.nimproject  for Nim
    """

    # TODO: add some info on the files the project has according to QtCreator
    # specs

    """
    {
    "files": ["main.py","test/jiala.py","main.qml","gui/jiala.qml"]
    }

        {
    "files": ["src/__main__.py", "src/__init__.py", "src/gui/__init__.py",
    "src/gui/main.qml", "src/modules/__init__.py"]
    }
    """

    print('add qtcreator project file')

    file_python_name = f'{projname}.pyproject'
    file_python = Path(file_python_name)
    file_nim_name = f'{projname}.nimproject'
    file_nim = Path(file_nim_name)

    pyproject_content = ''
    nimproject_content = ''

    if language == 'python':
        file_python.touch()
        file_python.write_text(pyproject_content)
    else:
        file_nim.touch()
        file_nim.write_text(nimproject_content)

