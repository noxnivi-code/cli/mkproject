#!/usr/bin/env python

# mkreadme.py
# (mkproject)
# (C) 202306 NoxNivi
# script to append content to the README.md file
#

from pathlib import Path


def add_readme_content(projname, rdmfile):
    """
    Adds a default README.md skeleton content to the recently created
    README.md file.

    Uses default Gitlab Markdown specs

    takes:
        projname: name of the project provided by the __main__
        rdmfile: the Path to the README.md file as created by __main__
    """
    readme_skel = (f"""# {projname}

### Project Status

## Description

## Screenshots

## Installation

## Usage

## Support

## License
THis software is released under the XXX license. A copy of said license is provided in this folder.
""")

    rdmfile.write_text(readme_skel)

