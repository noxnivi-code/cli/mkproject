#!/usr/bin/env python

# __main__.py
# mkproject v. 0.1.2
# (C) 202306 NoxNivi
# script to create a new project structure for:
#   Python QML apps
#   Python cli apps/scripts
#   Nim QML apps
#   Nim cli apps
#

# TODO: evaluate if we use this as a skeleton maker for shell scripts:
# for python:       #!/usr/bin/env python
# for bash:         #!/usr/bin/env bash
# for fish:         #!/usr/bin/env fish
# for nimscript:    #!/usr/bin/env nim

# TODO: evaluate creation of QtCreator project file

# base imports
import os
import sys
import subprocess

# import locals
import modules.mkreadme as mkrd
import modules.mkfiles as mkfl
import modules.mkqtcreator as mkqt

# from imports
from pathlib import Path

# makeproject name and version
_NAME = 'mkproject'
_VERSION = '0.2.1'

# red color codes for terminal error messages
COLOR_RED = '\33[31m'
COLOR_END = '\33[0m'
ERROR = COLOR_RED + '[Error]' + COLOR_END

COLOR_GREEN = '\33[93m'
SUCCESS = COLOR_GREEN + '[Success]' + COLOR_END

# message list
messages = []


def get_parameters(params):
    """
    Gets the parameters added from the command line and processes them
    - flags
    - application name
    Available flags:
        - pyqml:    python qml application
        - pycli:    python cli application/script
        - nimqml:   nim qml application
        - nimcli:   nim cli application
        - help:     display help

    Only 2 parameters are available, the flag and the app name.
    """
    flag_list = ['pyqml', 'pycli', 'nimqml', 'nimcli', 'help']
    error_list = []

    # first we check for the parameters
    if len(params) > 2:
        error_list.append(0)
        show_errors(0)
    if params[0] not in flag_list:
        error_list.append(1)
        show_errors(1)
    if params[0] in flag_list[0:4] and len(params) < 2:
        error_list.append(2)
        show_errors(2)
    if params[0] == flag_list[4] and len(params) > 1:
        error_list.append(3)
        show_errors(3)

    if params[0] != params[0].lower():
        print(params[0])
        print(params[0].lower)
        error_list.append(4)
        show_errors(4)

    if len(error_list) > 0:
        show_help()

    if params[0] == flag_list[4] and len(error_list) == 0:
        show_help()
        return 0
    
    if len(error_list) > 0:
        return 1
    else:
        return params


def show_errors(errorcode):
    """
    Shows the proper error message according to the error code 
    """

    if errorcode == 0:
        print(ERROR + ': too many arguments')
    if errorcode == 1:
        print(ERROR + ': flags not recognized')
    if errorcode == 2:
        print(ERROR + ': <project name> missing')
    if errorcode == 3:
        print(ERROR + ': <help> flag does not take parameters')
    if errorcode == 4:
        print(ERROR + ': bad flag sintax')


def show_help():
    """
    Shows the help of the application.
    """
    print('Basic <mkproject> usage information')
    print(_NAME + ' v.' + _VERSION)
    print('usage: mkproject <option> [project name]')
    print('options:')
    print('     pyqml: creates a Python + QML project named <project name>')
    print('     pycli: creates a Python CLI project named <project name>')
    print('     nimqml: creates a Nim + QML project named <project name>')
    print('     nimcli: creates a Nim CLI project named <project name>')
    print('     help: shows this help')


def set_project_structure(params):
    """
    Organizes the structure of the project accoring to its type
    """
    soft_type = ''
    language_type = ''
    project_name = params[1].lower()
    project_main_dir = project_name.capitalize()
    project_type = params[0]

    messages.append(project_name)
    messages.append(project_main_dir)
    messages.append(project_type)

    # print(project_name)
    # print(project_main_dir)
    # print(project_type)

    if ('qml' in project_type):
        soft_type = 'GUI'
    elif ('cli' in project_type):
        soft_type = 'CLI'

    if ('py' in project_type):
        language_type = 'python'
    elif ('nim' in project_type):
        language_type = 'nim'

    messages.append(soft_type)
    messages.append(language_type)

    # print(soft_type)
    # print(language_type)

    create_main_directory(project_main_dir, soft_type, language_type)


def create_main_directory(projmaindir, softtype, langtype):
    """
    Creates the common Project directory and files:
        ProjectName/
            AUTHORS
            LICENSE
            README.md
            TODO.md
            docs/
            help/ (if program is GUI, to store html help files)
            screenshots/
            src/
            auxfiles/
    This is the base structure for any app project.
    """
    main_dir = Path(projmaindir)
    main_dir.mkdir()

    os.chdir(main_dir)

    authors = Path('AUTHORS')
    set_license = Path('LICENSE')
    readme = Path('README.md')
    todo = Path('TODO.md')

    dir_docs = Path('docs')
    dir_help = Path('help')
    dir_aux = Path('auxfiles')
    dir_screenshots = Path('screenshots')
    dir_src = Path('src')

    authors.touch()
    mkfl.add_authors(projmaindir, authors)
    set_license.touch()
    readme.touch()
    mkrd.add_readme_content(projmaindir, readme)
    todo.touch()
    mkfl.add_todo(projmaindir, todo)

    dir_docs.mkdir()
    dir_screenshots.mkdir()
    dir_aux.mkdir()
    dir_src.mkdir()

    os.chdir(dir_src)

    dir_modules = Path('modules')
    dir_modules.mkdir()

    # go back to main project directory
    os.chdir('..')

    # initialize git
    git_init = subprocess.run('git init', check=True, shell=True,
                              capture_output=True)
    gignore_file = Path('.gitignore')
    gignore_file.touch()
    
    messages.append(git_init.stdout)

    if softtype == 'GUI':
        dir_help.mkdir()
        mkqt.add_project_file(projmaindir.lower(), langtype)
        create_qml_skel(dir_src, projmaindir.lower())
        create_program_skel(projmaindir)

    if langtype == 'python':
        create_python_skel(main_dir)
    else:
        create_nim_skel(main_dir)

    success_report(main_dir, messages)


def create_qml_skel(srcdir, projname):
    """
    Creates the gui folder and adds a main.qml file in that folder
    This method only adds what is common for Python and Nim GUI software
    """

    os.chdir(srcdir)

    dir_gui = Path('gui')

    main_qml = Path('main.qml')

    dir_gui.mkdir()
    os.chdir(dir_gui)
    main_qml.touch()
    mkfl.add_qml_main_skel(projname, main_qml)
    os.chdir('..')
    os.chdir('..')


def create_python_skel(projdir):
    """
    Creates python files and directories in the project's directory
    structure:
        zipapp/     directory at the root of ProjectName/
        __init__.py files in src/ and modules/
        __init__.py file in gui/ if it exists
        __main__.py file
    """
    messages.append('python skel')

    # print('python skel')

    # os.chdir(projdir)

    file_init = Path('__init__.py')
    file_main = Path('__main__.py')

    dir_zipapp = Path('zipapp')
    dir_zipapp.mkdir()

    os.chdir('src')
    file_init.touch()
    file_main.touch()
    # mkfl.add_python_main_skel(projdir, file_main)

    os.chdir('modules')
    file_init.touch()

    os.chdir('..')
    if Path('gui').exists():
        mkfl.add_pythonq_main_skel(projdir, file_main)
        os.chdir('gui')
        file_init.touch()
        os.chdir('..')
    else:
        mkfl.add_python_main_skel(projdir, file_main)
    os.chdir('..')


def create_nim_skel(projdir):
    """
    Creates nim files and directories in the project's directory structure:
        build/      directory at the root of ProjectName/
        main.nim    file in src/
    """
    messages.append('nim skel')

    # print('nim skel')

    # os.chdir(projdir)
    
    dir_build = Path('build')
    file_main = Path('main.nim')

    dir_build.mkdir()

    os.chdir('src')
    file_main.touch()

    if Path('gui').exists():
        mkfl.add_nimq_main_skel(projdir, file_main)
    else:
        mkfl.add_nim_main_skel(projdir, file_main)

    os.chdir('..')


def create_program_skel(projdir):
    """
    Creates the base structure for de SOFTDIR (executable software directory)
    only if we are going to build GUI software
    The skel follows this pattern:
        Programname.program/        - executable folder
            programname             - program executable
            programname.svg         - program icon
            Programname.desktop     - porgram desktop file
            help/                   - folder for program's help and images
                index.html          - help file
            other files             - other files/folders resources needed
    """
    messages.append('program skel')
    prog_softdir_name = projdir.capitalize() + '.program'
    prog_softdir_dir = Path(prog_softdir_name)
    prog_softdir_dir.mkdir()

    os.chdir(prog_softdir_dir)
    prog_desktop = Path(projdir.capitalize() + '.desktop')
    prog_help_dir = Path('help')
    prog_help_file = Path('index.html')

    # TODO: get the icon from the theme
    # fill the .desktop file

    prog_desktop.touch()
    mkfl.add_desktop_file(projdir, prog_desktop)
    prog_help_dir.mkdir()

    os.chdir(prog_help_dir)
    prog_help_file.touch()
    mkfl.add_help_file(projdir, prog_help_file)

    os.chdir('..')
    os.chdir('..')


def qtcreator_files():
    """
    Creates the needed QtCreator files for GUI software
    """
    messages.append('QtCreator files')
    # print('QtCreator files')
    

def success_report(data, msgs):
    """
    If everything went right, prints the success message
    """
    success_msg = (SUCCESS + f': <{data}> project skeleton has been created ')
    print(messages)
    print(success_msg)


def main(params):
    """
    launches the process
    """
    # print(params)
    project_data = get_parameters(params)
    if project_data != 1 and project_data != 0:
        set_project_structure(project_data)


if __name__ == '__main__':
    params = sys.argv[1:]
    if len(params) != 0:
        main(params)
    else:
        show_help()
