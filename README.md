# Mkproject

### Project Status
As all the elements of the **NoxNivi** 'set', this is a Work In Progress,
therefore development pace is slow.

## Description
This is a simple utility to create an opinionated project folder skeleton that
will serve as a base for the development of all the elements that conform the
NoxNivi set.

It also initializes a local Git repository, in order to push the project folder
to a Git service.

While targeted at the NoxNivi set, it may be useful for other projects too.

Creates the following Skeleton (project name = ProjectName):
```
    Projectname/
        .git/
        build/ (only if Nim language is selected)
        docs/
        screenshots/
        Projectname.softdir/  (only if the software is GUI)
            Projectname.desktop
            help/
                index.html
        src/
            gui/ (only if QML is added to flags)
                __init__.py (only if Python language is selected)
                main.qml
            modules/
                __init__.py (only if Python language is selected)
            __init__.py (only if Python language is selected)
            __main__.py (only if Python language is selected)
            main.nim (only if Nim language is selected)
        zipapp/ (only if Python language is selected)
        projectname.pyproject (only if Python and QML are selected)
        projectname.nimproject (only if Nim and QML are selected)
        AUTHORS
        LICENSE
        README.md
        TODO.md
```

Filling the files is left to the user.

## Screenshots

## Installation
**Requirements**
* Python 3

**General instructions**
This utility is released as a _zipapp_ therefore, user just needs to unpack the
compressed file, make it executable, and add it to the path.

**Arch Linux**
If user is running Arch or any Arch derived distro, can add the NoxNivi
repository and install it using `pacman`.

If user doesn't want to add the repository, can download the Arch package and
install locally.

**Haiku**
If user is running Haiku, there will be an HPKG released soon. In the mean
time, user can folow the General Instructions provided before. 

## Usage
From the terminal, call the `mkproject` command from the directory where the user
wants to create the project folder. The project skeleton will be created inside
that folder.

```
Basic <mkproject> usage information
mkproject v.0.1.0
usage: mkproject <option> [project name]
options:
     pyqml: creates a Python + QML project named <project name>
     pycli: creates a Python CLI project named <project name>
     nimqml: creates a Nim + QML project named <project name>
     nimcli: creates a Nim CLI project named <project name>
     help: shows this help
```

<project name> should be a single word without spaces

At the moment, only these options are available as these are the needed type of
projects that conform the NoxNivi set.

## Support

## License
THis software is released under the GPLv3 license. A copy of said license is provided in this folder.
