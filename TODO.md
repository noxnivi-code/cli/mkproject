# **Mkproject**'s TODO list

* Evaluate if can use this also for scriptig (bash/fish/nim/python)
* Improve error handling and add tests
* Complete the QtCreator module
* Format the output when the project skeleton has been created
* Add Python type hint?
* Improve README.md skeleton
* Improve the code
* Add man page?
